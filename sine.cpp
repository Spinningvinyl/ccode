// Change line 8 so only characters 32 to 126 print
#include <iostream>
#include <cmath>
using namespace std;

int main(){
	int x;
	float r ;
	float y;
	
	for(x = 0 ; x <= 360; x = x + 150){
		r = float(x * (3.14159265358723846 / 180.0));
		y = sin(float(r)) ;
		cout<< " I " << y << "\n";
	}
	return 0;
}
